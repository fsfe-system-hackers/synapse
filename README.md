<!--
SPDX-FileCopyrightText: 2021 Free Software Foundation Europe <https://fsfe.org>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# FSFE Synapse Ansible Playbook

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/synapse/00_README)
[![Build Status](https://drone.fsfe.org/api/badges/fsfe-system-hackers/synapse/status.svg)](https://drone.fsfe.org/fsfe-system-hackers/synapse)
[![REUSE status](https://api.reuse.software/badge/git.fsfe.org/fsfe-system-hackers/synapse)](https://api.reuse.software/info/git.fsfe.org/fsfe-system-hackers/synapse)
