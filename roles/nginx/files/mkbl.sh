#!/bin/sh
# modeled after https://github.com/perusio/nginx-spamhaus-drop, distributed as
#Copyright (C) 2012 António P. P. Almeida
#SPDX-License-Identifier: BSD-2-Clause
#
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
#Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
#Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
#THIS SOFTWARE IS PROVIDED BY AUTHOR AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
set -e
set -x

DROPURL=https://www.spamhaus.org/drop/drop.txt
EDROPURL=https://www.spamhaus.org/drop/edrop.txt
BLV6=https://www.spamhaus.org/drop/dropv6.txt

OUT=$(mktemp -d)
trap "rm -rf $OUT" INT EXIT

cd $OUT
wget $DROPURL $EDROPURL $BLV6

cat *.txt|sed -e '/^;/d' -e 's/\(^.*\) ; \(.*\)/\1 1; # \2/' > /etc/nginx/bl.conf
