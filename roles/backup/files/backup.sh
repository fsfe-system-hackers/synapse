#!/bin/sh

# SPDX-FileCopyrightText: 2021 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

echo "Backup Synapse database"
. ~root/.borg.passphrase
SDBB_DATE=$(date +%Y%m%d-%H%M)
SDBB_REPOSITORY=ssh://u124410-sub2@u124410.your-storagebox.de:23/./servers/ekeberg.fsfeurope.org
sudo -u postgres pg_dumpall |  borg create --verbose --stats --stdin-name synapse-db --compression lz4 $SDBB_REPOSITORY::${SDBB_DATE}-db -
