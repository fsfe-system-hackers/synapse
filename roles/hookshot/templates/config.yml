# SPDX-FileCopyrightText: 2024 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

bridge:
  domain: fsfe.org
  url: https://matrix.fsfe.org/
  port: 9993
  bindAddress: 0.0.0.0
passFile:
  # A passkey used to encrypt tokens stored inside the bridge.
  # Run openssl genpkey -out passkey.pem -outform PEM -algorithm RSA -pkeyopt rsa_keygen_bits:4096 to generate
  /data/passkey.pem
logging:
  # Logging settings. You can have a severity debug,info,warn,error
  level: debug
  colorize: true
  json: false
  timestampFormat: HH:mm:ss:SSS
listeners:
  - port: "{{ hookshot.webhooks_port }}"
    bindAddress: 0.0.0.0
    resources:
      - webhooks

generic:
  enabled: true
  enableHttpGet: false
  urlPrefix: https://matrix.fsfe.org/webhooks/
  userIdPrefix: _webhooks_
  allowJsTransformationFunctions: true
  waitForComplete: false

feeds:
  enabled: true
  pollConcurrency: 4
  pollIntervalSeconds: 600
  pollTimeoutSeconds: 30

experimentalEncryption:
  storagePath: /data/encryption

queue:
  # For encryption to work, must be set to monolithic mode and have a host & port specified.
  monolithic: true
  port: 6379
  host: hookshot_redis

permissions:
  - actor: "@weeman:ccc-ffm.de"
    services:
      - service: "*"
        level: "admin"
  - actor: "fsfe.org"
    services:
      - service: "*"
        level: "manageConnections"
